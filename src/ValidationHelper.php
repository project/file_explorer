<?php

namespace Drupal\file_explorer;

use App\Utils\ESUtils;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Helper class for validations.
 */
class ValidationHelper {

  private const RANGE_TYPES = ['gt', 'gte', 'lt', 'lte'];

  /**
   * Validator service.
   *
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  private $validator;

  /**
   * ValidationUtils constructor.
   *
   * @param ValidatorInterface $validator
   *   Validator service.
   */
  public function __construct(ValidatorInterface $validator) {
    $this->validator = $validator;
  }

  /**
   * Validates value with a constrain and throws an exception in case of errors.
   *
   * @param $value
   *   Value which should be validated.
   * @param $constrain
   *   Validation constrain.
   * @param $property
   *   Property path or machine name.
   *
   * @throws \Exception
   */
  public function validate($value, $constrain, $property = '') {
    $errors = $this->validator
      ->startContext()
      ->atPath($property)
      ->validate($value, $constrain)
      ->getViolations();

    if (count($errors) > 0) {
      throw new \Exception($errors);
    }
  }
  
  /**
   * Validates value with Assert\IsTrue() value.
   *
   * @param $value
   *   Value which should be validated.
   * @param string $message
   *   Validation message.
   * @param string $property
   *   Property path or machine name.
   */
  public function validateIsTrue($value, $message = '', $property = '') {
    $constrain = new Assert\IsTrue();
    $constrain->message = $message;
    $this->validate($value, $constrain, $property);
  }

  /**
   * Validates date time.
   *
   * @param $value
   * @param string $property
   * @param bool   $allowTimestamp
   * @param string $format
   * @param string $message
   */
  public function validateDateTime($value, $property = '', $allowTimestamp = TRUE, $format = ESUtils::DATE_TIME_FORMAT, $message = 'This value is not a valid datetime.') {
    if (is_numeric($value)) {
      $this->validateIsTrue($allowTimestamp, $message, $property);
      return;
    }

    $constrain = new Assert\DateTime();
    $constrain->format = $format;
    $constrain->message = $message;

    $this->validate($value, $constrain, $property);
  }

  /**
   * Validates date time range.
   *
   * @param $range
   * @param string $property
   * @param bool   $allowTimestamp
   * @param string $format
   * @param string $message
   */
  public function validateDateTimeRange($range, $property = '', $allowTimestamp = TRUE, $format = ESUtils::DATE_TIME_FORMAT, $message = 'This value is not a valid datetime.') {
    $this->validateIsTrue(is_array($range), 'This value should be an array.', $property);

    foreach ($range as $type => $date) {
      // TODO validate type
      $this->validateDateTime($date, "$property\[\"$type\"\]", $allowTimestamp, $format, $message);
    }
  }

  /**
   * Validates numeric range.
   *
   * @param $range
   * @param string $property
   * @param string $message
   */
  public function validateNumericRange($range, $property = '', $message = 'This property is not a valid numeric value.') {
    $this->validateIsTrue(is_array($range), 'This value should be an array.', $property);

    foreach ($range as $type => $value) {
      // TODO validate type
      $this->validateIsTrue(is_numeric($value), $message, "$property\[\"$type\"\]");
    }
  }

  /**
   * Validates number to be greater than another one.
   *
   * @param $value
   *   Value that should be checked.
   * @param string $property
   *   Property name.
   * @param $greater
   *   Grater value for validation.
   */
  public function validateGreaterThenNumber($value, $property = '', $greater = 0) {
    $this->validateIsTrue(is_numeric($value), 'This value should be numeric.', $property);
    $this->validate($value, new Assert\GreaterThan(['value' => $greater]), $property);
  }

}
