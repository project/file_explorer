<?php

namespace Drupal\file_explorer\Item;

/**
 * Defines an interface for FileExplorer items.
 */
interface FileExplorerItemInterface {

  /**
   * Search mode which returns both files and directories.
   *
   * @var string
   */
  const MODE_ALL = 'all';

  /**
   * Search mode which returns files only.
   *
   * @var string
   */
  const MODE_FILE = 'file';

  /**
   * Search mode which returns directories only.
   *
   * @var string
   */
  const MODE_DIRECTORY = 'directory';

  public function getInfo();

  public function create();

  public function update();

  public function delete();

}
