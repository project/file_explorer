<?php

namespace Drupal\file_explorer\Item;

use Drupal\file_explorer\Exception\FileExplorerException;
use Drupal\file_explorer\JsonErrorResponse;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;

class FileExplorerFolder extends FileExplorerItemBase implements FileExplorerItemInterface {


  public function getInfo(): array {
    $data = [];
    $finder = new Finder();

    // @TODO exclude some directories.
    $finder->in($this->getPath());
    $context = $this->getContext();
    if ($context) {
      $this->applyDepth($finder, $context);
      $this->applyMode($finder, $context);
    }

    foreach ($finder as $item) {
      $data[] = $item;
    }

    // @TODO implement paginator.

    return $data;
  }

  public function create()
  {
    // Try to create directory.
    $name = $this->getContext()->getDirName();
    if (!$name) {
      throw new FileExplorerException('The directory name can\'t be empty.');
    }

    $fullPath = $this->getPath() . $name;
    // @TODO add validation.
    if (file_exists($fullPath)) {
      throw new FileExplorerException(sprintf("Such directory '%s' already exists.", $fullPath));
    }

    $fs = \Drupal::service('file_system');
    if ($fs->mkdir($fullPath)) {
      $finder = new Finder();
      $realPath = $fs->realpath($this->getPath());
      $finder->in($realPath)->directories()->name($name);
      foreach ($finder as $item) {
        $data[] = $item;
      }

      return reset($data);
    }
    else {
      throw new FileExplorerException(sprintf("Can't create directory - %s.", $fullPath));
    }
  }

  public function update()
  {
    // TODO: Implement update() method.
  }

  public function delete() {
    // In First module version only empty directories are allowed to be deleted.
    // First check if directory is empty.
    $path = $this->getPath();
    if ($this->isDirEmpty($path)) {
      // Try do delete folder.
      if (!drupal_rmdir($path)) {
        throw new FileExplorerException(t("Can't delete folder."));
      }
    }
    else {
      throw new FileExplorerException(t("Folder doesn't empty and can't be deleted."));
    }
  }

  private function applyDepth(Finder $finder, FileExplorerItemContext $context) {
    $depth = $context->getDepth();
    if (is_numeric($depth)) {
      $finder->depth("<= $depth");
    }
  }

  private function applyMode(Finder $finder, FileExplorerItemContext $context) {
    $mode = $context->getMode();
    switch ($mode) {
      case self::MODE_FILE:
        $finder->files();
        break;

      case self::MODE_DIRECTORY:
        $finder->directories();
        break;
    }
  }

  /**
   * Check if directory is empty or not.
   *
   * @param string $dir
   *   Path to the directory.
   *
   * @return bool
   *   TRUE in case if directory is empty.
   */
  private function isDirEmpty($dir) {
    $handle = opendir($dir);
    while (false !== ($entry = readdir($handle))) {
      if ($entry != "." && $entry != "..") {
        return FALSE;
      }
    }
    return TRUE;
  }
}
