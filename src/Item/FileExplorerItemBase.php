<?php

namespace Drupal\file_explorer\Item;

/**
 * Provides a base implementation for FileExplorer Item.
 */
abstract class FileExplorerItemBase implements FileExplorerItemInterface {

  protected $path;

  protected $context;

  public function __construct(string $path, FileExplorerItemContext $context = NULL) {
    $this->path = $path;
    if (!$this->endsWith($this->path, DIRECTORY_SEPARATOR)) {
      $this->path .= DIRECTORY_SEPARATOR;
    }
    $this->context = $context;
  }

  public function getPath() {
    return $this->path;
  }
  public function getContext() {
    return $this->context;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function create() {}

  /**
   * {@inheritdoc}
   */
  public function update() {}

  /**
   * {@inheritdoc}
   */
  public function delete() {}

  protected function endsWith($haystack, $needle) {
    return substr_compare($haystack, $needle, -strlen($needle)) === 0;
  }

}
