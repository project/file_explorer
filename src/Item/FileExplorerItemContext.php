<?php

namespace Drupal\file_explorer\Item;

/**
 * Defines an interface for FileExplorer item context.
 */
class FileExplorerItemContext {

  /**
   * Depth of directory nesting to be displayed.
   *
   * @var int
   */
  protected $depth;

  /**
   * Display mode which determines what kind of items to show.
   *
   * Available values are: 'all', 'file' or 'directory'.
   *
   * @var string
   */
  protected $mode;

  protected $dirName;

  protected $files;

  /**
   * Sets directory nesting depth.
   *
   * @param int $value
   *   Nesting depth. Should be grater than or equals 0.
   */
  public function setDepth(int $value) {
    $this->depth = $value;
  }

  /**
   * Gets directory nesting depth.
   *
   * @return int
   *   Directory nesting depth.
   */
  public function getDepth() {
    return $this->depth;
  }

  /**
   * Sets display mode.
   *
   * @param string $value
   *   Display mode. Available values are: 'all', 'file' or 'directory'.
   */
  public function setMode(string $value) {
    $this->mode = $value;
  }

  /**
   * Gets display mode.
   *
   * @return string
   *   Display mode.
   */
  public function getMode() {
    return $this->mode;
  }

  public function setDirName($name) {
    $this->dirName = $name;
  }

  public function getDirName() {
    return $this->dirName;
  }

  public function setFiles($files) {
    $this->files = $files;
  }

  public function getFiles() {
    return $this->files;
  }

}
