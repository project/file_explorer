<?php

namespace Drupal\file_explorer\Item;

use Drupal\file_explorer\Exception\FileExplorerException;

class FileExplorerItemCreator {

  /**
   * @param string $path
   * @param FileExplorerItemContext $context
   * @param string $type
   * @return FileExplorerItemInterface
   * @throws \Exception
   */
  public static function createItem(string $path, FileExplorerItemContext $context = NULL, string $type = ''): FileExplorerItemInterface {
    if (is_file($path) || (is_dir($path) && strpos($type, 'multipart/form-data') === 0)) {
      return new FileExplorerFile($path, $context);
    }
    elseif (is_dir($path)) {
      return new FileExplorerFolder($path, $context);
    }
    else {
      // We cannot handle anything other (sockets, symbolic links, etc) than files and directories.
      $message = t('The %path is not of a recognized type.', ['%path' => $path]);
      throw new FileExplorerException($message);
    }
  }

}
