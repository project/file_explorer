<?php

namespace Drupal\file_explorer\Item;

use Symfony\Component\Finder\Finder;

class FileExplorerFile extends FileExplorerItemBase implements FileExplorerItemInterface {

  public function getInfo() {
    $finder = new Finder();
    $finder->append([$this->getPath()]);

    return $finder->getIterator()->current();
  }

  public function create()
  {
    // TODO: Implement create() method.

  }

  public function update()
  {
    // TODO: Implement update() method.
  }

  public function delete()
  {
    // TODO: Implement delete() method.
    return 'Coming soon...';
  }

}
