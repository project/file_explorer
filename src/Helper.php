<?php

namespace Drupal\file_explorer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a helper class.
 */
class Helper {

  /**
   * File system service.
   *
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a Helper object.
   *
   * @param FileSystemInterface $file_system
   *   The file system object.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * Gets real path for public files directory.
   *
   * @return string
   *   Real path for public files directory.
   */
  public function getFilesDirectory() {
    return $this->fileSystem->realpath(file_default_scheme() . "://");
  }

  /**
   * Checks if a user has an file explorer profile assigned for a file scheme.
   *
   * @param AccountInterface|NULL $user
   *   Active user.
   * @param string|null $scheme
   *   File scheme.
   *
   * @return bool
   */
  public static function access(AccountInterface $user = NULL, $scheme = NULL) {
    return (bool) static::userProfile($user, $scheme);
  }

  /**
   * Returns file explorer configuration profile for a user.
   *
   * @param AccountInterface|NULL $user
   *   Active user.
   * @param string|null $scheme
   *   File scheme.
   *
   * @return bool|EntityInterface|null
   *   File explorer profile entity.
   */
  public static function userProfile(AccountInterface $user = NULL, $scheme = NULL) {
    $profiles = &drupal_static(__METHOD__, []);
    $user = $user ?: \Drupal::currentUser();
    $scheme = isset($scheme) ? $scheme : file_default_scheme();
    $profile = &$profiles[$user->id()][$scheme];
    if (!isset($profile)) {
      // Check stream wrapper.
      if ($wrapper = \Drupal::service('stream_wrapper_manager')->getViaScheme($scheme)) {
        $storage = null;
        try {
          $storage = \Drupal::entityTypeManager()->getStorage('file_explorer_profile');
        }
        catch (\Exception $e) {
          // Logs an error.
          \Drupal::logger('file_explorer')->error($e->getMessage());
        }

        if ($storage && $user->id() == 1 && $profile = $storage->load('admin')) {
          return $profile;
        }
        $settings = \Drupal::config('file_explorer.settings');
        $roles_profiles = $settings->get('roles_profiles');
        $user_roles = array_flip($user->getRoles());
        // Order roles from more permissive to less permissive.
        $roles = array_reverse(user_roles());
        foreach ($roles as $rid => $role) {
          if (isset($user_roles[$rid]) && !empty($roles_profiles[$rid][$scheme])) {
            if ($profile = $storage->load($roles_profiles[$rid][$scheme])) {
              return $profile;
            }
          }
        }
      }

      $profile = FALSE;
    }

    return $profile;
  }

  /**
   * Returns processed profile configuration for a user.
   *
   * @param AccountInterface|null $user
   *   Active user.
   * @param string|null $scheme
   *   File scheme.
   *
   * @return array
   *   Profile configurations.
   */
  public static function userConf(AccountInterface $user = NULL, $scheme = NULL) {
    $configs = [];
    $user = $user ?: \Drupal::currentUser();
    $scheme = isset($scheme) ? $scheme : file_default_scheme();
    if ($profile = static::userProfile($user, $scheme)) {
      $conf = $profile->getConf();
      $conf['pid'] = $profile->id();
      $conf['scheme'] = $scheme;
      $configs = static::processUserConf($conf, $user);
    }

    return $configs;
  }

  /**
   * Processes raw profile configuration of a user.
   */
  public static function processUserConf(array $conf, AccountInterface $user) {
    // Convert MB to bytes.
    $conf['maxsize'] *= 1048576;

    // Check php max upload size.
    $phpMaxSize = file_upload_max_size();
    if ($phpMaxSize && (!$conf['maxsize'] || $phpMaxSize < $conf['maxsize'])) {
      $conf['maxsize'] = $phpMaxSize;
    }

    // Set root uri and url.
    $conf['root_uri'] = $conf['scheme'] . '://';

    // file_create_url requires a filepath for some schemes like private://
    $conf['root_url'] = preg_replace('@/(?:%2E|\.)$@i', '', file_create_url($conf['root_uri'] . '.'));

    $conf['token'] = $user->isAnonymous() ? 'anonymous' : \Drupal::csrfToken()->get('file_explorer');

    // Process folders.
    $conf['folders'] = static::processUserFolders($conf['folders'], $user);

    return $conf;
  }

  /**
   * Processes user folders.
   *
   * @param array $folders
   * @param AccountInterface $user
   *
   * @return array
   */
  public static function processUserFolders(array $folders, AccountInterface $user) {
    $ret = [];
    $token_service = \Drupal::token();
    $meta = new BubbleableMetadata();
    $token_data = ['user' => $user];
    foreach ($folders as $folder) {
      $path = $token_service->replace($folder['path'], $token_data, [], $meta);
      if (static::regularPath($path)) {
        $ret[$path] = $folder;
        unset($ret[$path]['path']);
      }
    }

    return $ret;
  }

  /**
   * Checks the structure of a folder path.
   * Forbids current/parent directory notations.
   *
   * @param string $path
   *
   * @return bool
   */
  public static function regularPath(string $path) {
    return is_string($path) && ($path === '.' || !preg_match('@\\\\|(^|/)\.*(/|$)@', $path));
  }

}
