<?php

namespace Drupal\file_explorer;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QueryFilter
 *
 *
 * Allows to retrieve filtered and validated query parameters.
 * You can define the query parameters in you route by in the 'queryParam'
 * of route 'defaults' like this:
 *   "defaults" = {
 *      "savable": true,
 *      "queryParams": {
 *        "rank": "bool",      <- You can specify only type this way.
 *        "page": {
 *          "type": "int",     <- Type of the parameter. See available types in `filtersMap`.
 *          "required": false, <- Whether this parameter is required.
 *          "array": "false",  <- Whether the value can be represented as an array.
 *          "validators": {    <- Array of constraints by which value should be validated.
 *            {
 *              "constraint": Assert\GreaterThanOrEqual::class, <- Don't forget to import constrains.
 *              "options": 10
 *            }
 *          },
 *          "filter" => FILTER_VALIDATE_INT <- Specify parameter filter particularly,
 *          "options" => {} <- Options for filter
 *        }
 *      }
 *    }
 */
class QueryFilter {

  /**
   * Map from variable type to filter.
   *
   * @var array
   */
  protected $filtersMap = [
    'default' => ['filter' => FILTER_DEFAULT, 'options' => []],
    'bool' => ['filter' => FILTER_VALIDATE_BOOLEAN, 'options' => []],
    'int' => ['filter' => FILTER_VALIDATE_INT, 'options' => ['default' => 0]],
  ];

  /**
   * @var Request
   */
  protected $request;

  /**
   * @var ValidationHelper
   */
  protected $validationHelper;

  public function __construct(RequestStack $requestStack, ValidationHelper $validationHelper) {
    $this->request = $requestStack->getCurrentRequest();
    $this->validationHelper = $validationHelper;
  }

  /**
   * Gets a single value from query.
   *
   * @param string $key
   *   Query key.
   * @param mixed $default
   *   Default value in case of the parameter hasn't been passed.
   * @param bool $validate
   *   Whether the values should be validated.
   *   Throws a validation exception in case of error.
   *
   * @return mixed
   *   Mixed value from query.
   */
  public function get($key, $default = NULL, $validate = TRUE) {
    $defaultInfo = [
      'required' => FALSE,
      'type' => 'default',
      'default' => NULL,
      'validators' => [],
      'array' => FALSE
    ];

    $routeInfo = $this->request->attributes->get('queryParams')[$key] ?? [];
    if (!is_array($routeInfo)) {
      // Instead of putting whole data into an array it's possible to only specify a query type.
      $routeInfo = ['type' => $routeInfo];
    }

    $routeInfo += $defaultInfo;

    if (!is_null($default)) {
      $routeInfo['default'] = $default;
    }

    $paramValue = $this->request->query->get($key, $routeInfo['default']);

    if ($validate) {
      $this->validationHelper->validateIsTrue(!$routeInfo['required'] || $this->request->query->has($key), "Query parameter $key is required.", $key);
      $this->validationHelper->validateIsTrue(in_array($routeInfo['type'], ['array', 'default']) || $routeInfo['array'] || !is_array($paramValue), "The value cannot be an array.", $key);

      if (is_array($paramValue) && $routeInfo['type'] != 'array') {
        foreach ($paramValue as $index => $value) {
          $paramValue[$index] = $this->filter($key, $value, $routeInfo);
        }
      }
      else {
        $paramValue = $this->filter($key, $paramValue, $routeInfo);
      }
    }

    return $paramValue;
  }

  /**
   * Returns all filtered query parameters.
   *
   * @return array
   *   Array of query parameters.
   */
  public function all() {
    $all = [];

    foreach ($this->request->query->keys() as $key) {
      $all[$key] = $this->get($key);
    }

    return $all;
  }

  /**
   * Returns current request.
   *
   * @return Request
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * Validates query property.
   *
   * @param string $key
   *   Property key.
   * @param $value
   *   Property value.
   * @param array $routeInfo
   *   Route info.
   */
  protected function validate($key, $value, $routeInfo) {
    foreach ($routeInfo['validators'] as $validator) {
      $constrain = new $validator['constraint']($validator['options']);
      $this->validationHelper->validate($value, $constrain, $key);
    }
  }

  /**
   * Filters and runs validation on query property.
   *
   * @param string $key
   *   Property key.
   * @param $value
   *   Property value.
   * @param array $routeInfo
   *   Route info.
   * @param bool $validate
   *   Whether validation should be ran.
   *
   * @return mixed
   *   Filtered value.
   */
  protected function filter($key, $value, $routeInfo, $validate = TRUE) {
    $type = $routeInfo['type'];
    $filter = $routeInfo['filter'] ?? $this->filtersMap[$type]['filter'];
    $options = $routeInfo['options'] ?? $this->filtersMap[$type]['options'];

    if (!is_null($routeInfo['default'])) {
      $options['default'] = $routeInfo['default'];
    }

    $filtered = filter_var($value, $filter, ['options' => $options]);

    $validate && $this->validate($key, $filtered, $routeInfo);
    return $filtered;
  }

}
