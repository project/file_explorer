<?php

namespace Drupal\file_explorer\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file_explorer\Helper;

/**
 * Base form for File explorer Profile entities.
 */
class FileExplorerProfileForm extends EntityForm {

  /**
   * Folder permissions.
   *
   * @var array
   */
  public $folderPermissions;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $profile = $this->getEntity();
    // Check duplication.
    if ($this->getOperation() === 'duplicate') {
      $profile = $profile->createDuplicate();
      $profile->set('label', $this->t('Duplicate of @label', ['@label' => $profile->label()]));
      $this->setEntity($profile);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $profile->label(),
      '#maxlength' => 64,
      '#required' => TRUE,
      '#weight' => -20,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [get_class($profile), 'load'],
        'source' => ['label'],
      ],
      '#default_value' => $profile->id(),
      '#maxlength' => 32,
      '#required' => TRUE,
      '#weight' => -20,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $profile->get('description'),
      '#weight' => -10,
    ];

    $conf = [
      '#tree' => TRUE,
    ];

    $conf['extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file extensions'),
      '#default_value' => $profile->getConf('extensions'),
      '#maxlength' => 255,
      '#description' => $this->t('Separate extensions with a space, and do not include the leading dot.') .
        ' ' . $this->t('Set to * to allow all extensions.'),
      '#weight' => -9,
    ];

    $maxsize = file_upload_max_size();
    $conf['maxsize'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => ceil($maxsize/1024/1024),
      '#step' => 'any',
      '#size' => 8,
      '#title' => $this->t('Maximum file size'),
      '#default_value' => $profile->getConf('maxsize'),
      '#description' => $this->t('Maximum allowed file size per upload.') . ' ' .
        $this->t('Your PHP settings limit the upload size to %size.', ['%size' => format_size($maxsize)]),
      '#field_suffix' => $this->t('MB'),
      '#weight' => -8,
    ];

    // @TODO override logic of adding new folders.
    $conf['folders'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Folders'),
      'description' => ['#markup' => '<div class="description">' .
        $this->t('You can use user tokens in folder paths, e.g. @tokens.', ['@tokens' => '[user:uid], [user:name]' ]) .
        ' ' . $this->t('Subfolders inherit parent permissions when subfolder browsing is enabled.') . '</div>'],
      '#weight' => 10,
    ];
    $folders = $profile->getConf('folders', []);
    $index = 0;
    foreach ($folders as $folder) {
      $conf['folders'][] = $this->folderForm($index++, $folder);
    }
    $conf['folders'][] = $this->folderForm($index++);
    $conf['folders'][] = $this->folderForm($index);
    $form['conf'] = $conf;

    return parent::form($form, $form_state);
  }

  /**
   * Returns folder form elements.
   *
   * @param $index
   * @param array $folder
   *
   * @return array
   *   Form array.
   */
  public function folderForm($index, array $folder = []) {
    $folder += ['path' => '', 'permissions' => []];
    $form = [
      '#type' => 'container',
      '#attributes' => ['class' => ['folder-container']],
    ];
    $form['path'] = [
      '#type' => 'textfield',
      '#default_value' => $folder['path'],
      '#field_prefix' => '&lt;' . $this->t('root') . '&gt;' . '/',
    ];
    $form['permissions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Permissions'),
      '#attributes' => ['class' => ['folder-permissions']],
    ];
    $perms = $this->permissionInfo();
    $form['permissions']['all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('All permissions'),
      '#default_value' => isset($folder['permissions']['all']) ? $folder['permissions']['all'] : 0,
    ];
    foreach ($perms as $perm => $title) {
      $form['permissions'][$perm] = [
        '#type' => 'checkbox',
        '#title' => $title,
        '#default_value' => isset($folder['permissions'][$perm]) ? $folder['permissions'][$perm] : 0,
        '#states' => [
          'disabled' => ['input[name="conf[folders][' . $index . '][permissions][all]"]' => ['checked' => TRUE]],
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check folders.
    $folders = [];
    foreach ($form_state->getValue(['conf', 'folders']) as $i => $folder) {
      $path = trim($folder['path']);
      if ($path === '') {
        continue;
      }
      // Validate path.
      if (!Helper::regularPath($path)) {
        return $form_state->setError($form['conf']['folders'][$i]['path'], $this->t('Invalid folder path.'));
      }
      // Remove empty permissions.
      $folder['permissions'] = array_filter($folder['permissions']);
      $folder['path'] = $path;
      $folders[$path] = $folder;
    }
    // No valid folders.
    if (!$folders) {
      return $form_state->setError($form['conf']['folders'][0]['path'], $this->t('You must define a folder.'));
    }
    $form_state->setValue(['conf', 'folders'], array_values($folders));

    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $profile = $this->getEntity();
    $status = $profile->save();
    $messenger = \Drupal::messenger();
    if ($status == SAVED_NEW) {
      $msg = $this->t('Profile %name has been added.', ['%name' => $profile->label()]);
      $messenger->addMessage($msg);
    }
    elseif ($status == SAVED_UPDATED) {
      $msg = $this->t('The changes have been saved.');
      $messenger->addMessage($msg);
    }
    $form_state->setRedirect('entity.file_explorer_profile.edit_form', ['file_explorer_profile' => $profile->id()]);
  }

  /**
   * Returns folder permission definitions.
   */
  public function permissionInfo() {
    if (!isset($this->folderPermissions)) {
      // @TODO rewrite this.
      //$this->folderPermissions = \Drupal::service('plugin.manager.imce.plugin')->permissionInfo();
    }
    return $this->folderPermissions;
  }

}
