<?php

namespace Drupal\file_explorer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;

/**
 * File explorer settings form.
 */
class FileExplorerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_explorer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['file_explorer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('file_explorer.settings');
    $form['roles_profiles'] = $this->buildRolesProfilesTable($config->get('roles_profiles') ?: []);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('file_explorer.settings');
    $roles_profiles = $form_state->getValue('roles_profiles');

    // Filter empty values.
    foreach ($roles_profiles as $rid => &$profiles) {
      if (!$profiles = array_filter($profiles)) {
        unset($roles_profiles[$rid]);
      }
    }

    $config->set('roles_profiles', $roles_profiles);
    $config->save();
    // Warning about anonymous access.
    if (!empty($roles_profiles[RoleInterface::ANONYMOUS_ID])) {
      $msg = $this->t('You have enabled anonymous access to the file manager. Please make sure this is not a misconfiguration.');
      \Drupal::messenger()->addWarning($msg);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns roles-profiles table.
   *
   * @param array $roles_profiles
   *
   * @return array
   */
  public function buildRolesProfilesTable(array $roles_profiles) {
    $table = ['#type' => 'table'];
    // Prepare roles.
    $roles = user_roles();
    $wrappers = \Drupal::service('stream_wrapper_manager')->getNames(StreamWrapperInterface::WRITE_VISIBLE);
    // Prepare profile options.
    $options = ['' => '-' . $this->t('None') . '-'];

    $profiles = [];
    try {
      $profiles = \Drupal::entityTypeManager()->getStorage('file_explorer_profile')->loadMultiple();
    }
    catch (\Exception $e) {
      // Logs an error.
      \Drupal::logger('file_explorer')->error($e->getMessage());
    }

    foreach ($profiles as $pid => $profile) {
      $options[$pid] = $profile->label();
    }

    // Build header.
    $fe_url = Url::fromRoute('file_explorer.overview')->toString();
    $table['#header'] = [$this->t('Role')];
    $default = file_default_scheme();
    foreach ($wrappers as $scheme => $name) {
      $url = $scheme === $default ? $fe_url : $fe_url . '/' . $scheme;
      $table['#header'][]['data'] = ['#markup' => '<a href="' . $url . '">' . Html::escape($name) . '</a>'];
    }

    // Build rows.
    foreach ($roles as $rid => $role) {
      $table[$rid]['role_name'] = [
        '#plain_text' => $role->label(),
      ];
      foreach ($wrappers as $scheme => $name) {
        $table[$rid][$scheme] = [
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => isset($roles_profiles[$rid][$scheme]) ? $roles_profiles[$rid][$scheme] : '',
        ];
      }
    }

    // Add description.
    $table['#prefix'] = '<h3>' . $this->t('Role-profile assignments') . '</h3>';
    $table['#suffix'] = '<div class="description">' . $this->t('Assign configuration profiles to user roles for available file systems. Users with multiple roles get the bottom most profile.') . ' ' . $this->t('The default file system %name is accessible at :url path.', ['%name' => $wrappers[file_default_scheme()], ':url' => $fe_url]) . '</div>';
    return $table;
  }

}
