<?php

namespace Drupal\file_explorer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a list of File explorer Profile entities.
 *
 * @see \Drupal\file_explorer\Entity\FileExplorerProfile
 */
class FileExplorerProfileListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['description'] = $this->t('Description');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $profile) {
    $row['label'] = $profile->label();
    $row['description'] = $profile->get('description');

    return $row + parent::buildRow($profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $profile) {
    $operations = parent::getDefaultOperations($profile);

    try {
      $url = $profile->toUrl('duplicate-form');
      $operations['duplicate'] = [
        'title' => t('Duplicate'),
        'weight' => 15,
        'url' => $url,
      ];
    }
    catch (\Exception $e) {
      // Logs an error.
      \Drupal::logger('file_explorer')->error($e->getMessage());
    }

    return $operations;
  }

}
