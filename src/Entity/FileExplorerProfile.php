<?php

namespace Drupal\file_explorer\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Defines the File explorer Profile entity.
 *
 * @ConfigEntityType(
 *   id = "file_explorer_profile",
 *   label = @Translation("File explorer Profile"),
 *   handlers = {
 *     "list_builder" = "Drupal\file_explorer\FileExplorerProfileListBuilder",
 *     "form" = {
 *       "add" = "Drupal\file_explorer\Form\FileExplorerProfileForm",
 *       "edit" = "Drupal\file_explorer\Form\FileExplorerProfileForm",
 *       "delete" = "Drupal\file_explorer\Form\FileExplorerProfileDeleteForm",
 *       "duplicate" = "Drupal\file_explorer\Form\FileExplorerProfileForm"
 *     }
 *   },
 *   admin_permission = "administer file explorer",
 *   config_prefix = "profile",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/media/file_explorer/{file_explorer_profile}",
 *     "delete-form" = "/admin/config/media/file_explorer/{file_explorer_profile}/delete",
 *     "duplicate-form" = "/admin/config/media/file_explorer/{file_explorer_profile}/duplicate"
 *   }
 * )
 */
class FileExplorerProfile extends ConfigEntityBase {

  /**
   * Profile ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Label.
   *
   * @var string
   */
  protected $label;

  /**
   * Description.
   *
   * @var string
   */
  protected $description;

  /**
   * Configuration options.
   *
   * @var array
   */
  protected $conf = [];

  /**
   * Returns configuration options.
   *
   * @param mixed $key
   *   Configuration key.
   * @param null $default
   *   Default configuration.
   *
   * @return array|mixed|null
   *   Configuration options.
   */
  public function getConf($key = NULL, $default = NULL) {
    $conf = $this->conf;
    if (isset($key)) {
      return isset($conf[$key]) ? $conf[$key] : $default;
    }
    return $conf;
  }

}
