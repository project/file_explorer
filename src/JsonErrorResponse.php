<?php

namespace Drupal\file_explorer;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Json error response.
 */
class JsonErrorResponse extends JsonResponse {

  /**
   * JsonErrorResponse constructor.
   *
   * @param mixed $message Error message which should be put into response
   * @param int   $status  The response status code
   * @param array $headers An array of response headers
   * @param bool  $json    If the data is already a JSON string
   */
  public function __construct($message = '', $status = 400, array $headers = [], $json = FALSE) {
    parent::__construct(['message' => $message], $status, $headers, $json);
  }

}
