<?php

namespace Drupal\file_explorer\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\file_explorer\Exception\FileExplorerException;
use Drupal\file_explorer\Helper;
use Drupal\file_explorer\Item\FileExplorerItemContext;
use Drupal\file_explorer\Item\FileExplorerItemCreator;
use Drupal\file_explorer\Item\FileExplorerItemInterface;
use Drupal\file_explorer\JsonErrorResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * File explorer controller.
 */
class FileExplorerController extends BaseController {

  /**
   * Constructs FileExplorerController.
   *
   * @param Helper $helper
   *   The file_explorer helper service.
   */
  public function __construct(Helper $helper, Request $request) {
    $this->helper = $helper;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_explorer.helper'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * File explorer overview page.
   *
   * @param string $scheme
   *   'public', 'private' or any other file scheme defined as the default.
   * @param Request $request
   *   Current request.
   *
   * @return array
   *   Page markup.
   */
  public function overview($scheme, Request $request) {
    $configs = $this->helper::userConf($this->currentUser(), $scheme);

    $element = [
      '#markup' => '<div id="root"></div>',
      '#attached' => [
        'library' => [
          'file_explorer/app',
        ],
      ],
    ];

    return $element;
  }

  /**
   * Checks access to File explorer overview page.
   *
   * @param string $scheme
   *   File scheme.
   *
   * @return AccessResult
   */
  public function checkAccess($scheme) {
    return AccessResult::allowedIf($this->helper::access($this->currentUser(), $scheme));
  }

  /**
   * Returns an administrative overview of File explorer Profiles.
   *
   * @param Request $request
   *   The current request object.
   *
   * @return array
   *   Page data.
   */
  public function adminOverview(Request $request) {
    // Build the settings form first.
    $output['settings_form'] =
      \Drupal::formBuilder()->getForm('Drupal\file_explorer\Form\FileExplorerSettingsForm');
    $output['settings_form']['#weight'] = 10;

    // Build profile list.
    $output['profile_list'] = [
      '#type' => 'container',
      'title' => ['#markup' => '<h2>' . $this->t('Configuration Profiles') . '</h2>'],
      'list' => $this->entityTypeManager()->getListBuilder('file_explorer_profile')->render(),
    ];

    return $output;
  }

  /**
   * File explorer entry point for REST API.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Array of files/directories normalized by FileInfoNormalizer.
   */
  public function entry() {
    $method = strtolower($this->request->getMethod());
    $method = "{$method}Handler";

    $response = new JsonErrorResponse("Wrong request.");;
    if (method_exists($this, $method) && is_callable([$this, $method])) {
      try {
        $response = $this->$method();
      }
      catch (FileExplorerException $e) {
        $response =  new JsonErrorResponse($e->getMessage());
      }
    }

    return $response;
  }

  /**
   * Handle GET request.
   *
   * The following query filters are used for searching files/directories:
   *   path: (string) `directory`/`path to the file` inside of public storage.
   *     Default value is '/' which means root of the drupal
   *     public storage folder.
   *   mode:      (string) One of the following values
   *                - \Drupal\file_explorer\Item\FileExplorerItemInterface::MODE_ALL
   *                - \Drupal\file_explorer\Item\FileExplorerItemInterface::MODE_FILE
   *                - \Drupal\file_explorer\Item\FileExplorerItemInterface::MODE_DIRECTORY.
   *   depth:     (string) Depth of the search. Default value is 0 - no depth,
   *              search only inside in the directory.
   *
   * @return JsonErrorResponse|JsonResponse
   *   Array of files/directories normalized by FileInfoNormalizer.
   * @throws
   */
  protected function getHandler() {
    $path = $this->getDirectoryFullPath();
    $mode = $this->getMode();
    $depth = $this->getDepth();
    $helper = $this->helper;

    // @TODO move validation to the Validator class.
    // Make sure that real search directory is still under the public
    // files directory. It'll cover situations like using '..' in path
    // in order to get on upper level of directory hierarchy.
    if (strpos($path, $helper->getFilesDirectory()) !== 0) {
      return new JsonErrorResponse("Directory value is invalid.");
    }

    if ($depth < 0) {
      return new JsonErrorResponse("Depth value can not be less then 0.");
    }

    $possible_modes = [
      FileExplorerItemInterface::MODE_ALL,
      FileExplorerItemInterface::MODE_FILE,
      FileExplorerItemInterface::MODE_DIRECTORY,
    ];
    if (!in_array($mode, $possible_modes)) {
      return new JsonErrorResponse("Mode value is invalid. Possible values are: " . implode(', ', $possible_modes));
    }

    $context = new FileExplorerItemContext();
    $context->setMode($mode);
    $context->setDepth($depth);

    $item = FileExplorerItemCreator::createItem($path, $context);
    $data = $item->getInfo();

    $serializer = \Drupal::service('serializer');
    $normalized_data = $serializer->normalize($data, 'json');

    $this->formatResponse($normalized_data);

    return new JsonResponse($normalized_data);
  }

  /**
   * Handle PUT request.
   *
   * @return JsonResponse
   */
  public function putHandler() {
    $data = [];

    // @TODO implement logic.
    // $file = file_move($source, $destination, FILE_EXISTS_ERROR);

    $this->formatResponse($data);

    return new JsonResponse($data);
  }

  /**
   * Handle POST request.
   *
   * @return JsonResponse
   * @throws
   */
  public function postHandler() {
    $directory = $this->getDirectory();
    // @TODO change scheme.
    $directory = "public://{$directory}";

    $content_type = strtolower($this->request->headers->get('content_type', ''));

    $context = new FileExplorerItemContext();
    $context->setDirName($this->getDirName());
    // @TODO review this
    // $context->setFiles($depth);

    $item = FileExplorerItemCreator::createItem($directory, $context, $content_type);
    $data = $item->create();

    $serializer = \Drupal::service('serializer');
    $normalized_data = $serializer->normalize($data, 'json');

    $this->formatResponse($normalized_data);

    return new JsonResponse($normalized_data, 201);

    if (strpos($content_type, 'multipart/form-data') === 0) {
      // Try to create files.
      $files = file_save_upload('f_e', [], $directory, NULL, FILE_EXISTS_ERROR);
      foreach (array_filter($files) as $file) {
        // @TODO Add file data to the $data variable.

        // Set status and save
        $file->setPermanent();
        $file->save();
      }

      $this->formatResponse($data);
    }

    return new JsonResponse($data, $status);
  }

  /**
   * Handle DELETE request.
   *
   * @return JsonResponse
   * @throws
   */
  public function deleteHandler() {
    $path = $this->getDirectoryFullPath();

    $item = FileExplorerItemCreator::createItem($path);
    $item->delete();

    return new JsonResponse([], 204);
  }

}
