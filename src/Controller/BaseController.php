<?php

namespace Drupal\file_explorer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file_explorer\Helper;
use Drupal\file_explorer\Item\FileExplorerItemInterface;
use Drupal\file_explorer\JsonErrorResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * File explorer base controller.
 */
class BaseController extends ControllerBase {

  /**
   * File explorer helper service.
   *
   * @var \Drupal\file_explorer\Helper
   */
  protected $helper;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  protected function getDirectoryFullPath() {
    $directory = $this->getDirectory();
    if (substr($directory, 0, 1) !== '/') {
      $directory = "/$directory";
    }

    return ($this->helper->getFilesDirectory() . $directory);
  }

  protected function getDirectory() {
    $dir = $this->request->get('path', '');
    if ($this->request->getMethod() === 'POST') {
      $content = json_decode($this->request->getContent());
      $dir = $content->path ?? '';
    }

    // Directory shouldn't start with '/'.
    $dir = ltrim($dir, '/');

    return $dir;
  }

  protected function getMode() {
    return $this->request->get('mode', FileExplorerItemInterface::MODE_ALL);
  }

  protected function getDepth() {
    return $this->request->get('depth', 0);
  }

  protected function getDirName() {
    $content = json_decode($this->request->getContent());

    return $content->name ?? '';
  }

  protected function formatResponse(&$data) {
    $data = [
      'data' => $data,
    ];
  }

}
