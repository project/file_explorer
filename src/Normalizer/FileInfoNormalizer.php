<?php

namespace Drupal\file_explorer\Normalizer;

use Drupal\file_explorer\Helper;
use Drupal\file_explorer\Item\FileExplorerItemInterface;
use \SplFileInfo;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Defines a FileInfoNormalizer class.
 */
class FileInfoNormalizer implements NormalizerInterface {

  /**
   * File explorer helper service.
   *
   * @var Helper
   */
  protected $helper;

  /**
   * Constructs FileExplorerController.
   *
   * @param Helper $helper
   *   The file_explorer helper service.
   */
  public function __construct(Helper $helper) {
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    return $this->normalizeFinderItem($object);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    return $data instanceof SplFileInfo;
  }

  /**
   * Normalizes finder item into array.
   *
   * @param SplFileInfo $item
   *   SplFileInfo file object.
   *
   * @return array
   *   Normalized file object into array with the following structure:
   *     type:     (string) Item type. One of the two values are possible:
   *                  - \Drupal\file_explorer\Helper::MODE_DIRECTORY
   *                  - \Drupal\file_explorer\Helper::MODE_FILE.
   *     name:     (string) Item name.
   *     path:     (string) Item path relatively to drupal files directory.
   *     readable: (bool) Whether item is readable.
   *     writable: (bool) Whether item is writable.
   *     created:  (int) Timestamp when item was created.
   *     modified: (int) Timestamp when item was modified last time.
   *     size:     (int) File size in bytes.
   */
  protected function normalizeFinderItem(SplFileInfo $item) {
    $helper = $this->helper;
    $path = substr($item->getRealPath(), strlen($helper->getFilesDirectory()));

    $data = [
      'type' => $item->isDir() ? FileExplorerItemInterface::MODE_DIRECTORY : FileExplorerItemInterface::MODE_FILE,
      'name' => $item->getBasename(),
      'path' => $path,
      'readable' => $item->isReadable(),
      'writable' => $item->isWritable(),
      'created' => $item->getCTime(),
      'modified' => $item->getMTime(),
      'size' => $item->getSize(),
    ];

    // @TODO add link to the file.
/*    if (!$item->isDir()) {
      $url = file_create_url('public:/' . $path);
      $data['link'] = $url;
    }*/

    if ($image_info = getimagesize($item->getRealPath())) {
      $data['is_image'] = TRUE;
      $data['width'] = $image_info[0];
      $data['height'] = $image_info[1];
    }
    else {
      $data['is_image'] = FALSE;
    }

    return $data;
  }

}
