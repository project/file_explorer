<?php

/**
 * @file
 * Install, update and uninstall functions for the file_explorer module.
 */

use Drupal\Core\StreamWrapper\StreamWrapperInterface;

/**
 * Implements hook_requirements().
 */
function file_explorer_requirements($phase) {
  // Drush might use finder which is installed in global composer, so there's no
  // point to check the existing of the Finder class. Instead we'll check
  // whether finder has been installed via composer into a project's vendor
  // directory.
  $has_finder = file_exists(DRUPAL_ROOT . '/../vendor/symfony/finder');
  $requirements['finder'] = [
    'title' => t('Finder'),
    'value' => $has_finder ? t('Enabled') : t('Not found'),
  ];

  if (!$has_finder) {
    $requirements['finder']['severity'] = REQUIREMENT_ERROR;
    $requirements['finder']['description'] = t('The File Explorer module requires <a href="https://symfony.com/doc/current/components/finder.html">The Symfony Finder Component</a>. You can install it with composer: "%composer"', ['%composer' => 'composer require symfony/finder']);
  }

  return $requirements;
}

/**
 * Implements hook_install().
 */
function file_explorer_install() {
  // Assign admin profile to administrators.
  $admin_roles = \Drupal::entityQuery('user_role')->condition('is_admin', TRUE)->execute();
  if ($admin_roles) {
    $config = \Drupal::configFactory()->getEditable('file_explorer.settings');
    $roles_profiles = $config->get('roles_profiles') ?: [];
    $wrappers = \Drupal::service('stream_wrapper_manager')->getWrappers(StreamWrapperInterface::WRITE_VISIBLE);
    foreach ($wrappers as $scheme => $info) {
      foreach ($admin_roles as $role) {
        $roles_profiles[$role][$scheme] = 'admin';
      }
    }
    $config->set('roles_profiles', $roles_profiles);
    $config->save(TRUE);
  }
}
